# Docker Compose example

This folder contains a Docker Compose example for the Snowplow realtime pipeline.

## Introduction

This Docker Compose example bundles the following Snowplow components in two distinct containers:

- [Scala Stream Collector][ssc]
- [Stream Enrich][se]

Additionally, they make use of [NSQ][nsq] topics to store events, the NSQ components are running
in four different containers:

- nsqd: the daemon in charge of receiving, queueing and delivering messages
- nsqlookupd: the daemon taking care of managing who produces and consumes what
- nsqadmin: a web UI to perform administrative tasks as well as giving an overview of the NSQ
topology, its web interface is available at port 4171
- nsq_pubsub: which lets you consume NSQ topics given a subscription channel you can create through
the nsqadmin UI. For example, to consume your bad rows, given a channel named `bad_channel` you can
hit the `http://127.0.0.1:8081/sub?topic=bad&channel=bad_channel` endpoint

## Usage

Once you have modified the configuration files to your liking, you can launch those two components
with:

```bash
$ docker swarm init # only required the first time
$ docker stack deploy -c docker-compose.yml snowplow-realtime
```

To stop the components:

```bash
$ docker stack rm snowplow-realtime
```

https://github.com/wurstmeister/kafka-docker

## S3-loader

- cd s3-loader-config

```bash
$ docker-compose build
```


```bash
$ docker-compose up -d --force-recreate
```


## Note
Elastic search docker need config
```bash
$ sudo sysctl -w vm.max_map_count=262144
```

## Docs
- https://github.com/shazChaudhry/docker-elastic
- https://medium.com/@sece.cosmin/docker-logs-with-elastic-stack-elk-filebeat-50e2b20a27c6

## Enrichments
`stream-enrich/enrichments/*.json file`
- https://github.com/snowplow/snowplow/wiki/Configurable-enrichments

## How to use:
https://logz.io/blog/mixpanel-analytics-elk-stack/
