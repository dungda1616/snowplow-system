# https://github.com/snowplow/snowplow-elasticsearch-loader/blob/develop/examples/config.hocon.sample
# Sources currently supported are:
# "kinesis" for reading records from a Kinesis stream
# "stdin" for reading unencoded tab-separated events from stdin
# If set to "stdin", JSON documents will not be sent to Elasticsearch
# but will be written to stdout.
# "nsq" for reading unencoded tab-separated events from NSQ
source = nsq

# Where to write good and bad records
sink {
  # Sinks currently supported are:
  # "elasticsearch" for writing good records to Elasticsearch
  # "stdout" for writing good records to stdout
  good = "elasticsearch"

  # Sinks currently supported are:
  # "kinesis" for writing bad records to Kinesis
  # "stderr" for writing bad records to stderr
  # "nsq" for writing bad records to NSQ
  # "none" for ignoring bad records
  bad = "stderr"
}

# "good" for a stream of successfully enriched events
# "bad" for a stream of bad events
# "plain-json" for writing plain json
enabled = "good"

# The following are used to authenticate for the Amazon Kinesis sink.
#
# If both are set to "default", the default provider chain is used
# (see http://docs.aws.amazon.com/AWSJavaSDK/latest/javadoc/com/amazonaws/auth/DefaultAWSCredentialsProviderChain.html)
#
# If both are set to "iam", use AWS IAM Roles to provision credentials.
#
# If both are set to "env", use environment variables AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY
aws {
  accessKey = iam
  secretKey = iam
}

# config for NSQ
nsq {
  # NSQ
  channelName = "elastic_channel"
  nsqdHost = "nsqd"
  # HTTP port for nsqd
  nsqdPort = 4151
  # Host name for nsqlookupd
  nsqlookupdHost = "nsqlookupd"
  # HTTP port for nsqd
  nsqlookupdPort = 4161
}

kinesis {
  # "LATEST": most recent data.
  # "TRIM_HORIZON": oldest available data.
  # "AT_TIMESTAMP": Start from the record at or after the specified timestamp
  # Note: This only affects the first run of this application on a stream.
  initialPosition = "initialPosition"

  # Need to be specified when initial-position is "AT_TIMESTAMP".
  # Timestamp format need to be in "yyyy-MM-ddTHH:mm:ssZ".
  # Ex: "2017-05-17T10:00:00Z"
  # Note: Time need to specified in UTC.
  initialTimestamp = "initialTimestamp"

  # Maximum number of records to get from Kinesis per call to GetRecords
  maxRecords = "10"

  # Region where the Kinesis stream is located
  region = ""

  # "appName" is used for a DynamoDB table to maintain stream state.
  # You can set it automatically using: "SnowplowElasticsearchSink-${sink.kinesis.in.stream-name}"
  appName = "none"
}


# Common configuration section for all stream sources
streams {
  inStreamName = "in_stream"

  # Stream for enriched events which are rejected by Elasticsearch
  outStreamName = "out_stream"

  # Events are accumulated in a buffer before being sent to Elasticsearch.
  # The buffer is emptied whenever:
  # - the combined size of the stored records exceeds byteLimit or
  # - the number of stored records exceeds recordLimit or
  # - the time in milliseconds since it was last emptied exceeds timeLimit
  buffer {
    byteLimit = 0 # Not supported by NSQ, will be ignored
    recordLimit = 0
    timeLimit = 0 # Not supported by NSQ, will be ignored
  }
}

elasticsearch {

  # Events are indexed using an Elasticsearch Client
  # - endpoint: the cluster endpoint
  # - port: the port the cluster can be accessed on
  #   - for http this is usually 9200
  #   - for transport this is usually 9300
  # - username (optional, remove if not active): http basic auth username
  # - password (optional, remove if not active): http basic auth password
  # - shardDateFormat (optional, remove if not needed): formatting used for sharding good stream, i.e. _yyyy-MM-dd
  # - shardDateField (optional, if not specified derived_tstamp is used): timestamp field for sharding good stream
  # - max-timeout: the maximum attempt time before a client restart
  # - ssl: if using the http client, whether to use ssl or not
  client {
    endpoint = "elasticsearch"
    port = "9200"
    #username = "{{elasticsearchUsername}}"
    #password = "{{elasticsearchPassword}}"
    #shardDateFormat = "{{elasticsearchShardDateFormat}}"
    #shardDateField = "{{elasticsearchShardDateField}}"
    maxTimeout = "300"
    maxRetries = 2
    ssl = "false"
  }

  # When using the AWS ES service
  # - signing: if using the http client and the AWS ES service you can sign your requests
  #    http://docs.aws.amazon.com/general/latest/gr/signing_aws_api_requests.html
  # - region where the AWS ES service is located
  aws {
    signing = "false"
    region = "ap-southeast-1"
  }

  # index: the Elasticsearch index name
  # type: the Elasticsearch index type
  cluster {
    name = "nsq_elastic_loader"
    index = "main"
    clusterType = "single"
  }
}

#monitoring {
#  snowplow {
#    collectorUri = "elastic-loader"
#    collectorPort = "9400"
#    ssl = "{{sslMonitoring}}"
#    appId = "{{appId}}"
#    method = "{{method}}"
#  }
#}
